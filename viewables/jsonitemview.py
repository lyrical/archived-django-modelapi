#!/usr/bin/env python

import types

from django import http
from jsonview import JSONView

class JSONItemView(JSONView):
    """A django class based view that can return a specific item from the
    designated table and jsonize it. The model is passed in by the urlpattern
    as is the id of the item to find."""

    def render_to_response(self, context):
        """Overwrite render_to-response in order to spit out our objects."""

        model = context['params']['model']
        foreign_keys = context['params']['foreign_keys']

        # ditto if there's no id
        if not context['params'].has_key('id'):
            items = model.objects.all()
        else:
            id = context['params']['id']
            try:
                items = model.objects.get(id=id)
            except:
                return http.HttpResponseBadRequest

        return JSONView.render_to_response(self, items, foreign_keys)
