django-modelapi
===============

A project to enable serialization of data to and from django models, using inspection. *modelapi* reflects on a django models file, adding url patterns for adding, deleting, and editing data in the database.

LICENSE: MIT

Requirements
============
Python 2.5+
Django 1.3+

Installation
============

For now, check this out of github, and include it as a module in your project. I maintain a tree called _portable_ for things like this.  

Usage
=====

In *urls.py*

from django-modelapi import ModelAPI
modelfile = "/the/full/path/to/your/django/model.py"
mapi = ModelAPI('someurlroot', modefile)
mapi.generate\_api()
urlpatterns = mapi.urlpatterns

The rest is described in your newly loaded django urls.  For each model in your models from from above three urls will be created. In the example below - let's assume you have a model named potato:

1. /someurlroot/potato/
2. /someurlroot/potato/new
3. /someurlroot/potato/delete
4. /someurlroot/potato/update

*Deleting Items*

For the _delete_ url, simply finish the URL off with the ID of the object to be deletd (eg. .../delete/15).

*Inserting New Items*
For both _new_ and _update_ views, pass in a query string with name/value pairs, based on the column name of the database, and that's what will happen.  

Example: /someurlroot/potato/update/15?type=yukon\_gold # this will set the type column to the value "yukon\_gold".

*Finding Items*

To find an item, the same name/value idea applies, just to item #1. In other wordsL;
/someurlroot/potato?type=tuber will return all objects where the type column value is _tuber_.  As a bonus, in case you're only interested in retrieving items, the _find_ url supports a format specifier, so that your object can be retrieved as *json*, *xml*, *python*, or *yaml*.  Simply ensure _format_=_xml_ (for example) to retreive an xml serialized version of your query.  The default is JSON.
