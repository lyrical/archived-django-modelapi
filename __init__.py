#!/usr/bin/env python

import imp
import inspect
import os
import types

from django.conf.urls.defaults import *

from viewables import *

class ModelAPI:
    """This class represents the entry point into our sdk creater.
    The idea is to create some sort of automated sdk interface.  
    Foreign keys are traversed automatically - though I have plans on
    integrating wadofstuff.
    """
    urlpatterns = []

    def __init__(self, appname, model_file):
        """Constructor
        model_file - The full path to the model file for the django application."""
        _mpath = os.path.abspath(model_file).split("/")
        self._APPNAME = appname
        try:
            mpath = _mpath[-2]
        except IndexError:
            err = """Provide an exact path to the module, no "/" was found!"""
            raise IndexErr, err
        try:
            _module = _mpath[-1].split(".")[-2]
        except IndexError:
            err = "The module must be a script file, ending in .py"
            raise IndexError, err

        module = imp.load_source("%s.%s" %(mpath, _module), model_file)
        members = inspect.getmembers(module)
        classes = [m for m in members if inspect.isclass(m[1])]
        self.class_list = classes

    def generate_api(self):
        for cl in self.class_list:
            try:
                _imported = cl[1]()
            except Error, e:
                print "Ignoring error, printing it, and continuing in the loop:"
                print e
                continue

            foreign_keys = self._get_foreign_items(cl)
            self._generate_catchall(cl, foreign_keys)
            self._generate_by_item(cl, foreign_keys)

            # add on classes
            self._generate_new_item(cl)
            self._generate_update_item(cl)
            self._generate_delete_item(cl)

    def _get_foreign_items(self, cls):
        """Return a list of fields that are foreign keys for the
        specified class."""
        stack = []
        cc = inspect.getmembers(cls[1])
        for c in cc:
            if c[1].__class__.__name__ == "ReverseSingleRelatedObjectDescriptor":
                stack.append(c[0])
        return stack

    def _generate_by_item(self, cls, foreign_keys):
        """Activate the URL to support querying objects basd on their ID"""
        self.urlpatterns += patterns('',
            (r'%s/sdk/%s/(?P<id>\d+)/$' %(self._APPNAME, cls[0].lower()), JSONItemView.as_view(), {'model' : cls[1], 'foreign_keys' : foreign_keys}),
        )

    def _generate_catchall(self, cls, foreign_keys):
        """Generate the catch all, for this url.  This is just the name of the
        model, such that a query string can now be sent, and we can
        build up an awesome api."""
        self.urlpatterns += patterns('',
            url(r'%s/sdk/%s/$' %(self._APPNAME, cls[0].lower()), JSONItemView.as_view(), {'model' : cls[1], 'foreign_keys' : foreign_keys}, 
                name="%s" %cls[0].lower()),
        )

    def _generate_new_item(self, cls):
        """Generate a url that can be used to add new items to the database, via a 
        json call."""
        self.urlpatterns += patterns('',
            url(r'%s/sdk/%s/new/$' %(self._APPNAME, cls[0].lower()), JSONNewView.as_view(), {'model' : cls[1]}, name="%s_add" %cls[0].lower()),
        )

    def _generate_update_item(self, cls):
        """Generates a url that can be used to update a specific item in the database.
        This applies, only by an item id, at least until I have a bigger need."""
        self.urlpatterns += patterns('',
            (r'%s/sdk/%s/update/(?P<id>\d+)/$' %(self._APPNAME, cls[0].lower()), JSONUpdateView.as_view(), {'model' : cls[1]}),
        )

    def _generate_delete_item(self, cls):
        """Generates a url that can be used to delete a specific item in the database.
        This applies, only by an item id, at least until I have a bigger need."""
        self.urlpatterns += patterns('',
            (r'%s/sdk/%s/delete/(?P<id>\d+)/$' %(self._APPNAME, cls[0].lower()), JSONDeleteView.as_view(), {'model' : cls[1]}),
        )
