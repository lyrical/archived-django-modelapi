#!/usr/bin/env python

import types

from django import http
from jsonview import JSONView

class JSONNewView(JSONView):
    """This class will add things to the database, via json.  You'll get a
    failure, omn failure."""

    def render_to_response(self, context):
        """Overwrite render_to-response in order to spit out our objects."""

        model = context['params']['model']

        get_items = dict(self.request.GET.items())
        item = model()
        for key, value in get_items.iteritems():
            try:
                setattr(item, key, value)
            except ValueError:
                setattr(item, "%s_id" %key, value)

        # TODO: support errors in json string
        try:
            item.save()
        except:
            return http.HttpResponseServerError()
        return JSONView.render_to_response(self, item)
