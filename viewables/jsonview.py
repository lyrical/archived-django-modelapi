#!/usr/bin/env python

import types

from django import http
from django.core import serializers
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView

class JSONView(TemplateView):
    """JSONItemView extends the TemplateView, just so that our response
    is encoded."""

    def render_to_response(self, item, foreign_keys=[]):

        format = "json"
        if self.request.GET.has_key('format'):
            format = self.request.GET['format']

        if format == "xml": 
            mimetype = "application/xml"
        elif format == "yaml":
            mimetype = "application/yaml"
        elif format == "python": 
            mimetype = "application/python"
        else: # default json
            format = "json"
            mimetype = "application/javascript"

        # damn django-1.3 bug means that you can't simply call dict()
        # on the tuple, instead the dict has to be created by hand
        get_items = {}
        for g in self.request.GET.items():
            get_items[str(g[0])] = g[1]

        # filter unless we cause an exception (single item), otherwise
        # add to list so that serialization doesn't shit itself
        try:
            # TODO filter out fields that don't exist
            items = item.filter(**get_items)
        except AttributeError:
            items = [item]

        # direct call to serializer rather than the more modern method
        # is needed because of an issue with json in django 1.3 and 
        # ManyToManyField columns
        _s = serializers.get_serializer(format)
        return http.HttpResponse(_s().serialize(items, relations=foreign_keys), mimetype=mimetype)
