#!/usr/bin/env python

import types

from django import http
from jsonview import JSONView

class JSONUpdateView(JSONView):
    """This class can update a specific item via a json call"""

    def render_to_response(self, context):
        """Overwrite render_to-response in order to spit out our objects."""

        model = context['params']['model']

        # ditto if there's no id
        id = context['params']['id']
        try:
            item = model.objects.get(id=id)
        except:
            return http.HttpResponseBadRequest

        get_items = dict(self.request.GET.items())
        for key, value in get_items.iteritems():
            setattr(item, key, value)

        # TODO: support errors in json string
        item.save()
        return JSONView.render_to_response(self, item)
