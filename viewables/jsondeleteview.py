#!/usr/bin/env python

import types

from django.shortcuts import get_object_or_404
from django import http
from jsonview import JSONView

class JSONDeleteView(JSONView):
    """A django class based view that can delete a specific item from the
    designated table. The model is passed in by the urlpattern
    as is the id of the item to find."""

    def render_to_response(self, context):
        """Overwrite render_to-response in order to spit out our objects."""

        model = context['params']['model']

        id = context['params']['id']
        item = get_object_or_404(model, id=id)
        item.delete()
        return JSONView.render_to_response(None)
